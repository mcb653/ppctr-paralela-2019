# Título de práctica
Autor

Fecha DD/MM/YYYY

## Prefacio

Objetivos conseguidos

Breve descripción de lo que te ha parecido la práctica. Aprendizajes, dificultades, cosas interesantes, qué te hubiera gustado hacer o mejorarías, qué conceptos te gustaría haber practicado de C++/OMP, opinión sobre el informe, etc

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VirtualBox:
  - Ubuntu Server 18.04 64bits actualizado
  - Sin entorno gráfico
  - 5363 MiB RAM
  - 5 Cores
  - Flags: VT-x/AMD-V, Nested Paging, PAE/NX, Hyper-V Paravirtualization
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU AMD FX-8320E
  - CPU 8 cores: 1 socket, 2 threads/core, 4 cores/socket.
  - CPU flags: sse, sse2, ssse3, fma, fma4 y avx
  - CPU @3200 MHz (fijada con cpufreq usermode)
  - CPU: L2: 2MiB, L3: 8MiB
  - 16 GiB RAM
  - Trabajo y benchmarks sobre SSD
  - 100 procesos en promedio antes de ejecuciones
  - Sin entorno gráfico durante ejecuciones
  - ArchLinux 64bits actualizado
- ... resto de información relevante

## 2. Diseño e Implementación del Software

En este apartado se describirá la estructura del software desarrollado justificando las  decisiones que se hayan tomado en su diseño.

Si se cree necesario, se puede aportar código fuente, pero lo normal será describir los objetos que se propongan para desarrollar la práctica y la funcionalidad de los mismos, ya que el código fuente se entregará por separado.

Si se quiere enseñar código fuente se puede aportar en bloques de código simplificado (solo lo relevante, tal y como yo hago en las diapositivas del Tema 2) o indicar las líneas de código del fichero en cuestión. Ojo con la segunda opción ya que cada vez que toques el código fuente podrán cambiar.

Ejemplo de código:

```c++
#define NTHREADS 3

void worker_function(){}

void run(){
  std::thread threads[NTHREADS];
  for (auto i=0; i<NTHREADS; ++i){
    threads[i] = std::thread(worker_function);
  }
  // ...
  for (auto i=0; i<NTHREADS; ++i){
    threads[i].join();
  }
}
```

```c++
switch(x){
// ...
case 0: // mt
// ...
case 1: // st
// ...
}

thread(&solveArray::sum,...)
thread(&solveArray::xorr,...)
```

Puedes apoyarte en esquemas o gráficos para definir tu diseño o interacciones entre hilos (recomendable formato png):

![](images/enginecl_tiers_patterns.png)

<img src="images/translation_opencl_enginecl.png" width="500" style="display:block; margin:0 auto;"/>

## 3. Metodología y desarrollo de las pruebas realizadas

Descripción detallada del proceso de benchmarking, captura de resultados y generación de gráficas. Todo lo necesario para comenzar el análisis.

Aquí se describirán las pruebas realizadas, su intención y los resultados obtenidos. Se incluirá el desarrollo de programas de prueba adicionales si han sido necesarios.

Podéis incluir vuestros scripts de ejecución en el propio informe (o llevarlos a Anexos si lo crees conveniente), así como indicar cómo los habéis utilizado y por qué:

Algunos códigos de ejemplo:

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
file=results1.log
touch $file
for season in 1 2; do
    for benchcase in st1 mt1; do
        echo $benchcase >> $file
        for i in `seq 1 1 10`;
        do
            printf "$i:" >> $file # iteration
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
```

**runner.sh:**

```sh
#!/usr/bin/env bash
size=10240
size2=10240000
op=xor
nthreads=8
case "$1" in
    st1)
        ./p1 $size $op
        ;;
    mt1)
        ./p1 $size $op --multi-thread $nthreads
        ;;
    st2)
        ./p1 $size2 $op
        ;;
    mt2)
        ./p1 $size2 $op --multi-thread $nthreads
        ;;
esac
```

Puedes incrustar gráficas de resultados (recomendable formato png):

![](images/benchmarks_speedup.png)

<img src="images/benchmarks_worksizes.png" style="display:block; margin:0 auto;"/>

## 4. Discusión

En este apartado se aportará una valoración personal de la práctica, en la que se destacarán aspectos como las dificultades encontradas en su realización y cómo se han resuelto.

También se responderá a las cuestiones formuladas si las hubiere.

## 5. Propuestas optativas

### Propuesta 1

Descripción de la propuesta.

Resultados y Análisis de la propuesta.

### Propuesta 2

...

## Anexos

**NOTA 1**: El objetivo del informe es presentar la resolución de la práctica para que se puedan comprender las soluciones propuestas. Por lo tanto, se valorará la claridad y la concisión en la exposición y las justificaciones.

**NOTA 2**: Este documento es sólo un modelo, se puede usar la estructura y formato que cada cual estime oportuno.
