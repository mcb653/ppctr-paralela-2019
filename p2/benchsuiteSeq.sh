#!/usr/bin/env bash
file=resultlog.csv
touch $file
for season in 1 2; do
    for benchcase in seqR1 seqR2 ;do
        echo $benchcase >> $file
        for i in `seq 1 1 20`;
        do
            ./runnerSeq.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
