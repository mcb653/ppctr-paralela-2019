#include <stdio.h>
#include <math.h>

#include "p2.hpp"
#include <time.h>
#include <sys/time.h>
int main(int argc, char* argv[])
{
  long numIts=atol(argv[1]);
  struct timeval inicio,fin;
  gettimeofday(&inicio,NULL);
  Data data = parse(numIts);
  sequential(data);
  gettimeofday(&fin,NULL);
  double t = (double) ((fin.tv_sec  + fin.tv_usec/1000000.0) - (inicio.tv_sec + inicio.tv_usec/1000000.0)) ;
  printf("%f\n",t);
}

int attach(Data* data, long iterations){
  if (iterations <= 0){
    return -1;
  }
  Aggregator* agg = (Aggregator*)malloc(sizeof(Aggregator));
  agg->separator = (double)1/(double)iterations;
  agg->A = 1.5;
  agg->B = 2.4;
  agg->C = 1.0;
  data->agg = agg;
  return 0;
}

Data parse(long numIts){
  // parse 'long' and set numIts
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  if (attach(&d, numIts) != 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}

void sequential(Data data){
	int i;
  int jump = 2.4;
	double value;
  double tmp = 0;
  double ellipse;

  int id;
  double x;
  for (i=0; i<data.numIts; i=i+1) {
    x = (i+(double)1/(int)2.0) * data.gap;
    tmp = tmp + pow(2,2)/(1+pow(x, 2));
  }
  value = tmp * data.agg->separator;
  ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

	//printf("ellipse: %f \n", ellipse);
}
