/**
Miguel Casamichana Bolado

PRÁCTICA 2

**/
#include <stdio.h>
#include <math.h>
#include <thread>
#include <mutex>
#include <omp.h>
#include <time.h>
#include <sys/time.h>

void sequential(long numIters, int k);//Prototipo de la función sequential

#if BINARIO == 1
std::mutex mut; //Se declara un mutex en caso de ser una ejecución paralela
#endif
double value;
long numIts;

int main(int argc, char* argv[]){
  if(argc==1){
    return -1;
  }
  numIts=atol(argv[1]);//Obtenemos el número de iteraciones
  if (numIts <=0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  }
  struct timeval inicio,fin;
  int nthreads=1;
  /**
    Comienza la regíon de interés de cada ejecución (sea paralela, secuencial u openMP)
  **/
  gettimeofday(&inicio,NULL);
  #if BINARIO == 0
  sequential(numIts,0);
  #endif
  /**
    Si es una ejecución paralela ya sea OpenMP o no, se declara el número de threads con los que operar,
    ya sea pasado como parámetro o como variable de entorno.
  **/
  #if BINARIO == 1 || BINARIO == 2
  if(argc==3){
    nthreads=atoi(argv[2]);//Como parámetro
  }else{
    nthreads=atoi(getenv("OMP_NUM_THREADS"));//Como variable de entorno
  }
  #endif
  #if BINARIO == 1
    long numItsThread=numIts/nthreads; //Establecemos el número de iteraciones de cada hilo
    long resto=numIts-nthreads*numItsThread; //Calculo el resto en caso de haberlo
    std::thread threads[nthreads];//Reservo espacio para un array de threads.
    threads[0]= std::thread(sequential,resto+numItsThread,0);//Asigno al primer thread su parte más la del resto en caso de haberle.
    for(auto i=1; i<nthreads; ++i){
      threads[i] = std::thread(sequential,numItsThread,resto+i*numItsThread);//Asigno al resto de hilos su parte
    }
    for (auto i=0; i<nthreads; ++i){
      threads[i].join(); //Hago un join a cada hilo ya que ya habrían finalizado su parte.
    }
  #endif
  /**
    Parte en OpenMP (BINARIO=2 en el Makefile)
  **/
  #if BINARIO == 2
  double value;
  double tmp;
  int i=-1;
  omp_set_num_threads(nthreads);//Se declaran el número de threads a utilizar
  /**
    Cuando comenzamos la región paralela declaramos a privada la i para que cada hilo tenga su propia i
    dentro del bucle, compartidas numIts y value ya que son variables compartidas por ambos hilos; y hacemos
    un reduction con operación de suma sobre tmp ya que es la variable sobre la que van a operar los hilos
    realizando una escritura y lectura sobre ella (tmp = tmp + ...).
  **/
  #pragma omp parallel private(i) shared(numIts, value) reduction(+:tmp) num_threads(nthreads)
  {
    int aux=omp_get_thread_num();//Cada hilo obtiene su propio id para poder trabajar dentro del bucle
    /**
      Cabe destacar que se podría aplicar un omp for estático que simplificría bastante el problema. Sin embargo,
      como no está permitido aplicarlo en esta práctica he tenido que asignar en función del id del hilo la
      división del trabajo.
    **/
    for(i=i+aux;i<numIts;i=i+nthreads){
      tmp = tmp + 4/(1+((i+0.5)/numIts)*((i+0.5)/numIts));
    }
    value=value+tmp;
  }
#endif
double ellipse = value /numIts * 4.082482905; //Cálculo final de la elipse
gettimeofday(&fin,NULL);
double t = (double) ((fin.tv_sec  + fin.tv_usec/1000000.0) - (inicio.tv_sec + inicio.tv_usec/1000000.0)) ;
printf("%f\n",t);
//printf("ellipse: %f \n", ellipse);
}
/**
  Método de operación utilizado para calcular ciertos parámetros de la elipse.

  Parámetros: numIters el cual es un lon que representa el número de iteraciones a realizar
  y k el cual es un entero que indica el índice sobre el que se empieza a operar.
**/
void sequential(long numIters, int k){
  double tmp; //Variable auxilar necesaria para el cálculo posterior
  for (int i=k; i<k+numIters; i++) {
    tmp = tmp + 4/(1+((i+0.5)/numIts)*((i+0.5)/numIts));
  }
  //Si es secuencial (BINARIO = 0 en el Makefile) no se requiere de exclusión mutua ya que opera todo el hilo master.
  #if BINARIO == 0
  value=tmp;
  #endif
  /**
    Si es una ejecución paralela (BINARIO = 1 en el Makefile) se requiere de un mutex para
    garantizar exclusión mutua sobre la variable sobre la que se escribe. También, se podría hacer
    atómica la variable global.
  **/
  #if BINARIO == 1
  {
    std::lock_guard<std::mutex> guard(mut);
    value=value+tmp;
  }
  #endif
}
