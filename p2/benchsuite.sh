#!/usr/bin/env bash
file=resultlog.csv
touch $file
for season in 1 2; do
    for benchcase in seq1 cpp1-2 cpp1-4 cpp1-8 omp1-2 omp1-4 omp1-8 seq2 cpp2-2 cpp2-4 cpp2-8 omp2-2 omp2-4 omp2-8 seq3 cpp3-2 cpp3-4 cpp3-8 omp3-2 omp3-4 omp3-8; do
        echo $benchcase >> $file
        for i in `seq 1 1 20`;
        do
            ./runner.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
