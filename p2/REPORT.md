# Práctica 2

Miguel Casamichana Bolado

Fecha 16/12/2019

## Prefacio

Objetivos conseguidos: Repasar conceptos sobre porgramación básica de C, utilizar correctamente una región paralela en openMP, repasar como paralelizar código en C y descubrir como openMP puede simplificar el paralelizar programas básicos.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Datos de mi sistema aportados en la práctica 1. Recordemos que con cpufreq-info podemos ver que governor tengo sobre cada core y a cuanta frecuencia.

Establezco el governor de mis 4 cores a userspace con:
  ```sh
  sudo cpufreq-set -c 0 -g userspace
  sudo cpufreq-set -c 1 -g userspace
  sudo cpufreq-set -c 2 -g userspace
  sudo cpufreq-set -c 3 -g userspace
  ```

y establezco la frecuencia de ellos a 2.60 GHz con:
  ```sh
  sudo cpufreq-set -c 0 -f 2.60GHz
  sudo cpufreq-set -c 1 -f 2.60GHz
  sudo cpufreq-set -c 2 -f 2.60GHz
  sudo cpufreq-set -c 3 -f 2.60GHz
  ```
Es decir, core a core vamos cambiando los governors a userspace y ponemos la frecuencia deseada, que en este caso es 2.60GHz.

Realizo 100 ejecuciones de calentamiento con:
    ```sh
    for i in `seq 1 1 100`; do ./build/omp 200000000 4;done
    ```
Como expliqué en la práctica 1, debido a las grandes fluctuaciones de mi frecuencia, uso 20 ejecuciones por estación de las cuales la primera es de warm up por lo que se desecha y se sacan resultados de las otras ejecuciones restantes. Además, de usar 2 estaciones para comprobar que ejecutando lo mismo 2 veces, en 2 estaciones, no hay grandes variaciones y de esta forma se consolidan un poco más mis conclusiones sobre estos datos.

## 2. Diseño e Implementación del Software

El código utilizado para la realización de esta práctica consta de una función llamada sequential que básicamente hace una serie de cálculos necesarios para la obtención del valor de la elipse, y un main en el cual se contemplan tres tipos de ejecuiones según la compilación condicional: una ejecución secuencial, una paralela usando C y otra ejecución paralela usando directivas de OpenMP.

***optimizaciones***

Cabe destacar que a la hora de optimizar el código he eliminado todas las estructuras (structs) que no se usaban en el archivo hpp. Además, alguna se repetía como puede ser el gap, u otros datos como una serie de doubles los cuales eliminé e introduje a mano. De esta forma no se tiene que acceder al struct para obtener esos datos y elimino código redundante. Tambíen, para realizar ciertas operaciones necesarias para el cálculo de la elipse se hacían varias llamadas a funciones de la librería matemática como pow y sqrt las cuales también podían ser suprimidas e introducidas a mano para evitarnos el acceder a librerías, estructuras de datos, etc que nos implicaban que hubiese un gran overhead. Es por eso, que al final del cálculo de la elipse se ve un "4.082482905". Se podría decir, que prácticamente he eliminado el archivo hpp entero debido al gran overhead producido por todo lo mencionado recientemente. (Sigue habiendo constancia de ese archivo para poder ejecutar el secuencial original y hacer mediciones sobre él)

***sequential***

Se trata del método con el que obtenemos el "value" necesario para el cálculo final de la elipsecdel problema. A este se le pasan dos parámetros:
  -numIters: se trata de un long el cual representa cuantas iteraciones se tienen que realizar con la llamada a esta función.
  -k: índice sobre el que empezar a operar dentro del bucle. En caso de ser secuencial será 0 siempre.

En caso de ser una ejecución paralela, cada thread tendrá una k distinta y se trabajará en función de esa k para lograr una correcta división del trabajo.

Una vez acabada todas las iteraciones del bucle, si es una ejecución secuencial se guardará el resultado en la variable global y se acabará el método. Sin embargo, en caso de ser paralela se creará un lock para obtener el mutex y que cuando un hilo tenga ese mutex (global también) pueda leer y escribir sin que otro hilo le "pise" la variable global value, es decir, se crea el lock para garantizar la exclusión mutua de dicha variable entre hilos, ya que todos ellos trabajan sobre ella.

***main***

Este main utiliza compilación condicional para distinguir entre distintas ejecuciones, ya que aunque haya partes comunes, muchas otras son únicas para cada tipo de ejecución.

Lo primero que se hace en el main es obtener mediante el parseo de argumentos el número de iteraciones introducido como parámetro en la ejecución y declarar los structs necesarios para la medición de tiempo de ejecución con la API gettimeofday. Posteriormente, se inicializa el strcut inicio de dicha API explicado en la memoria de la P1.

En caso de ser una ejecución secuencial, es decir que BINARIO sea igual a 0, se realiza la ejecución secuencial para ello tan solo se llama al método sequential introduciendo como parámetros el número de iteraciones y un 0 indicando que tiene que realizar en el bucle esas iteraciones y que empieza desde 0 ya que al ser secuencial no hay paralelismo y lo realiza todo el hilo master. Finalmente, en las últimas líneas de código se calcula el valor final de la elipse.

En caso de ser una ejecución paralela o paralela aplicando OpenMP, es decir que BINARIO sea igual a 1 (cpp) o a 2 (omp), se obtienen el número de threads. Estos pueden ser o bien pasados como parámetro o como variable de entorno. En el segundo caso se realiza lo siguiente para obtenerlo:

```sh
nthreads=atoi(getenv("OMP_NUM_THREADS"));
```

Posteriormente, comenzaría el código dedicado exclusivamente a la parte paralela. Se comienza obteniendo el número de iteraciones para cada hilo, el resto en caso de haberle, se declara un array de threads de tamaño igual a nthreads y se asigna de forma estática la parte del resto al primer hilo (en caso de haber resto) como se puede ver aquí:

```sh
threads[0]= std::thread(sequential,resto+numItsThread,0)
```
Acto seguido, al resto de threads se les asigna la misma función, pero sin que tengan en cuenta el resto ya que es el primer hilo el que opera con ello.

Después, se hace un join a todos los hilos una vez finalicen su trabajo. Al igual que en el caso secuencial, en las últimas líneas de código se calucla el valor final de la elipse.

A continuación, comenzaría la parte dedicada a una ejecución aplicando directivas de OpenMP.

Lo primero que hago es declarar el número de threads con los que voy a trabajar posteriormente:

```sh
omp_set_num_threads(nthreads)
```
Una vez hecho esto, procedo a crear la región paralela con la que se va a dividir el trabajo entre hilos para lograr un paralelismo al igual que en el caso de C.

Cuando comenzamos dicha región declaramos como private la i para que cada hilo tenga su propia i dentro del bucle, como shared numIts y value ya que son variables compartidas por ambos hilos; y como reduction tmp ya que hacemos un reduction con operación de suma sobre tmp porque que es la variable sobre la que van a operar los hilos realizando una escritura y lectura sobre ella (tmp = tmp + ...) y es necesario que haya exclusión mutua.

Finalmente, al igual que en las otras dos ejecuciones, en las últimas líneas de código se calcula el valor de la elipse.

Por último, se inicializa el strcut fin de la API explicado en la memoria de la P1, se calcula el tiempo de ejecución de la región de interés y se printea.

## 3. Metodología y desarrollo de las pruebas realizadas

Como mencioné antes y expliqué antes, he usado 2 estaciones con 20 ejecuciones por estación de cada tipo de operación. Además, no voy a poner menciones a fragmentos de los scripts ya que son muy similares a los de la práctica 1 ya que están basados todos en el mismo script proporcionado en el repositorio original como ejemplo.

Para dar permisos a estos scripts hago un

```sh
chmod +x nombrebench.sh
```

Usando mis binarios ya optimizados tengo en cuenta tres tamaños para ver si realmente hay ganancia en ejecuciones grandes frente a ejecuciones más pequeñas. En el runner.sh se puede apreciar que he usado como tamaños 2000, 20000000 y uno mucho más grande aún 2000000000.

Cabe destacar que para mis próximas 3 gráficas no he especificado correctamente la leyenda de los gráficos, por lo que explicaré brevemente cada una de ellas:
  seq, cpp y omp corresponde a cada tipo de ejecución, el número a continuación es el número de hilos y si termina en s2 quiere decir que es de la segunda estación. Por ejemplo, omp4s2 quiere decir que es una ejecución de 4 hilos usando directivas de openMP y que es de la segunda estación. Además, la segunda estación la represento para que se aprecie que los valores de una estación y otra son similares para consolidar la coherencia de mis datos y gráficos.

Tras la ejecución de este primer script, podemos proceder a analizar los resultados obtenidos y vemos como respecto a mi secuencial optimizado con ejecuciones muy pequeñas como 2000 iteraciones, es más eficaz un uso de la ejecución secuencial como se puede ver aquí:

![](images/grafica-t2000.png)

Esto se debe a que al ser ejecución muy pequeña, no merece la pena crear estructuras de sincronización debido a que aunque el overhead de esto sea muy pequeño, para unas ejecuciones tan pequeñas en proporción dejaría de ser despreciable. Es decir, para este caso, crear hilos y sincronizarlos acarrea un overhead tan grande(en proporción para este caso) que hace que la ejecución secuencial sea más óptima. Además, como se puede ver en el gráfico, a más hilos, menor speed-up ya que se crean más estructuras de sincronización.

Para unos tamaño más grandes vemos que pasa algo totalmente distinto al caso anterior: se ve un claro speed-up de la ejecución paralela sobre la secuencial como se puede apreciar aquí:

![](images/grafica-t20000000.png)

y aquí:

![](images/grafica-t2000000000.png)

Esto se debe a que para un número de iteraciones tan grande, el usar estructuras de sincronización como los hilos permite una división del trabajo que hace que el tiempo de ejecución sea menor ya que el trabajo no recae sobre un solo hilo haciendo que haya cores del pc que no estén en uso. Sin embargo, al paralelizar, se permite aprovechar tiempo de los otros cores que estaban en "desuso" (ya que no están en desuso, sino que posiblemente realizando tareas en segundo plano por lo que para esta ejecución en concreto a no ser que sea paralela, no están implicados) y conseguir así acabar antes.

Esto se puede apreciar viendo como con dos hilos consigo un speed-up de casi 2 ya sea una ejecución paralela usando C++ o usando directivas de openMP. Además, respecto a las paralelizaciones de C++ y openMP ambas son muy similares respecto a su speed-up sobre la ejecución secuencial. Esto se debe a que aunque el overhead producido al sincronizar y usar estructuras de sincronización de cada tipo de paralelización sea distinto, en una ejecución tan grande, eso es totalemente despreciable ya que ambas consiguen que se divida el trabajo de una forma muy similar por lo que al fin y al cabo su speed-up sobre el secuencial es similar.

Tambíen, he de mencionar que mi procesador tiene dos cores físicos, cada uno con dos cores lógicos que comparten recursos por lo que no siempre se garantiza que ambos cores lógicos de cada core físico pueda acceder a dichos recursos compartidos lo que hace que uno espere a otro. Como se puede ver aquí https://en.wikichip.org/wiki/amd/ryzen_3/3200u se muestra toda la información de mi procesador. Cada core físico, tiene una memoria de instrucciones de nivel 1 (L1I) compartida para cada thread lógico del thread físico como se puede apreciar aquí:

![](images/cores.png)

lo cual no garantiza que se llegue al speed-up teórico. Es decir, esto justifica el por qué con 4 hilos no obtengo un speed-up de entre 1,5 a 2 sobre la ejecución con 2 hilos. (Sino que obtengo un speed-up de 1,1. Comprobé en otros pcs de compañeros con este mismo código como me salía "a ojo" ejecutando unas 5-10 una ejecución con 2 hilos y otra con 4 con este mismo tamaño un speed-up coherente. Es decir, entre 1,7 y 1,9)

Además, creé otro benchsuite auxiliar con su respectivo runner llamado benchsuiteSeq.sh y runnerSeq.sh con la única finalidad de ver como mejoraba mi secuencial optimizado sobre el proporcionado en los archivos originales del repositorio. Para comprobar esto, utilicé 2 tamaños usados en el script anterior: 2000 y 20000000.

Los resultados de mis optimizaciones mencionadas anteriormente son los siguientes:

![](images/graficaSeqs.png)

y sobre las ejecuciones paralelas:

![](images/graficaSeqMalo.png)

Se puede apreciar, como justificando lo mencionado anteriormente acerca de mis optimizaciones he logrado un speed-up bastante alto entre mi ejecución optimizada y la original llegando a obtener incluso un speed-up de casi 10 para un número de iteraciones grande con mi secuencial frente al secuencial original. Obviamente, sobre las paralelas también tengo un speed-up alto de casi 20 (con 2 hilos), ya que si tenía un speed-up frente a mi secuencial optimizado de casi 2 y éste a su vez tiene una ganancia de casi 10 sobre el original, la lógica te incita a pensar que debes tener una ganancia de casi 20 del paralelo con 2 hilos al secuencial malo por ejemplo.

Adjunto el archivo .csv sobre el que he realizado mis cálculos y gráficas en caso de que sea necesario que se revise éste.

## 4. Discusión

*Explica cómo has hecho los benchmarks y qué metodología de medición has usado
(ROI+gettimeofday, externo).*

Como he hecho los benchmarks viene explicado en el apartado anterior. Sin embargo, aún no he explicado por que uso ROI+gettimeofday.

La razón del uso de esta metodología viene dada por que en este caso quiero comparar la región de interés del programa. Es decir, no quiero comparar el tiempo de ejecución total del programa cuando es una ejecución secuencial frente a cuando es una paralela. Sino, que quiero comparar la diferencia de la parte secuencial frente a la paralela. Pongamogs por ejemplo que hay una parte del código común que me tarda 2 segundos, la parte de ejecución secuencial me tarda 3 y la paralela 1. No es lo mismo un speed-up de 3/1 que de 5/3. Es por esta razón por la que quiero comprobar el tiempo de ejecución de la región de interés y no la total.
