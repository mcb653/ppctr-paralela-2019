# Práctica 4

Miguel Casamichana Bolado

Fecha 20/12/2019

## Prefacio

Objetivos conseguidos: comprender como funcionan las task y la utilidad de éstas.

La forma de realizar esta práctica me ha parecido entretenida en el sentido de que he tenido que ir prácticamente línea por línea del código haciendo un man o usando Zeal para comprender el problema o práctica en cuestión.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas

## 1. Sistema

Definido en la práctica 1, en su sección de Sistema.

## 2. Diseño e Implementación del Software

Debido a que la función fgauss es la que más carga de trabajo conlleva dentro de esta práctica, en un principio intenté paralelizar dicha función. Sin embargo, como ésta no es paralelizable como se nos indicó en clase decidí paralelizar el bucle donde se llamaba a esta función.

Por ello, decidí crear una región paralela que englobase mediante sus scopes todo el bucle do while usando un pragma omp parallel especificando como variables compartidas todas las utilizadas en ese bucle.

Además, como es un while que depende de una condición para acabar, es decir, que en un principio no sabemos cuando va a acabar he decidido implementar un pragma omp task que me englobe la llamada a la función fgauss por lo mencionado anteriormente. De esta forma, se me crean n tareas de las cuales cada hilo va cogiendo una a una.

La condición del while, es básicamente que mientras que no se haya llegado al final del archivo o quede algo por leer se hacen iteraciones a ese bucle while.

Además, previamente a crear la tarea, aplicando un pragma omp master hago que el hilo master sea el encargado de crear dichas tareas. Esto está condicionado en función de si size es igual a 1, es decir, si se ha leído algo (fread).

Tras esto, la tarea es creada y hacemos i++ indicando que parte del buffer se ha llenado un poco mas. Es decir, si el buffer es de tamaño 8 e i llega a 8, el buffer se habría llenado por lo que posteriormente habría que vacíarlo (i=0).

Para guardar el contenido del buffer se hace uso de la función fwrite que escribe en out el contenido del buffer una vez todas las tareas creadas hayan acabado. Esto se consigue con la directiva pragma omp taskwait, la cual actúa como barrera para las tareas como mencioné antes.

Además, fuera del bucle, en caso de que no se haya llenado el buffer del todo (i!=seq) y se haya llegado al fin del archivo, es decir, se ha leído todo, se acaba escribiendo la parte que faltaba en caso de haberla.

## 3. Metodología y desarrollo de las pruebas realizadas

He usado el comando diff para comprobar que los outs generados por la ejecución secuencial y la paralela sean iguales.

Cabe destacar que el benchsuite.sh y el runner.sh son muy parecidos a los de prácticas anteriores ya que he usado el mismo número de estaciones, mismo número de ejecuciones por estación y mismo benchmark pero con distintos benchcases por lo que es bastante similar a anteriores. Sigo guardando los datos en un archivo .csv para poder generar las gráficas directamente desde ahí.

He establecido en el generator el valor max a 20. Además, mis tamaños del buffer son 1, 5 y 20(ya que el max es 20) para comprobar como con tamaño 1 tendría que dar un speed-up similar al secuencial y como con tamaños de buffer mayores habría cierta ganancia respecto al secuencial.

Los speed-up obtenidos se pueden ver aquí:

![](images/grafica.png)

A la vista de estos resultados, se puede apreciar como con tamaño de buffer igual a 1 es similar a realizarlo de forma secuencial ya que como siempre se cumple la condición del taskwait debido a que se espera a que acabe una sola tarea, por lo que habría un solo hilo realizando las n tareas. Sin embargo, con un tamaño de buffer superior a 1 el speed-up se mejoría ya que habría menos barreras (menos taskwaits) lo cual permitiría que se pueda paralelizar permitiendo una división de trabajo entre hilos. Además, se puede apreciar como a mayor tamaño de buffer más speed-up.
