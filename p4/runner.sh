case "$1" in
  seq)
  ./build/video_task_seq
  ;;
  par1)
  ./build/video_task 1
  ;;
  par5)
  ./build/video_task 5
  ;;
  par20)
  ./build/video_task 20
  ;;
esac
