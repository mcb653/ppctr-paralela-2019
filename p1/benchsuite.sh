#!/usr/bin/env bash
file=resultlog.csv
touch $file
for season in 1 2; do
    for benchcase in st1 mt1-2 mt1-4 mt1-8 st2 mt2-2 mt2-4 mt2-8 ; do
        echo $benchcase >> $file
        for i in `seq 1 1 20`;
        do
            ./runner.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
