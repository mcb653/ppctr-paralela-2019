/**
Miguel Casamichana Bolado

PRÁCTICA 1

**/
#include <cstdio>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <thread>
#include <math.h>
#include <mutex>
#include <time.h>
#include <sys/time.h>
#include <condition_variable>
#include <atomic>
/**
Comentar o descomentar los dos siguientes
define en caso de querer activar o desactivar
el modo FEATURE_LOGGER, el modo DEBUG y/o el
modo FEATURE_OPTIMIZE
**/

//#define FEATURE_LOGGER
//#define DEBUG
//#define FEATURE_OPTIMIZE

/**
Prototipo de funciones (sum, sub y xor)
**/
void funSum(double* lista,long frag, long index, int resto, int num);
void funSub(double* lista,long frag, long index, int resto, int num);
void funXor(double* lista,long frag, long index, int resto, int num);
/**
Declaración de la longitud de la lista, número de hilos,
variable auxiliar para el cálculo de las operaciones, mutex y
de la variable booleana que condiciona si hay resto o no
**/
long longitud;
int nthreads=1;
#ifndef FEATURE_OPTIMIZE
double auxGlbl;
bool repite = false;
#endif
std::mutex mut;
/**
Variables globales atómicas usadas en el modo de
funcionamiento FEATURE_OPTIMIZE
**/
#ifdef FEATURE_OPTIMIZE
std::atomic<double> auxGlbl(0);
//std::atomic<bool>repite(false);
bool repite=false;
#endif
/**
Protipo de la función del logger y declaración de las variables
necesarias para la correcta ejecución del modo FEATURE_LOGGER
**/
#ifdef FEATURE_LOGGER
void procesa(std::condition_variable *cv2,int tip);
int contador=0;
double listaAux[12];
std::condition_variable cv;
bool ready=false;
bool ready2=false;
double result=0;
#endif

int main(int argc, char* argv[]){
  /**
  Comprobamos que el número de parámetros introducidos sea correcto
  **/
  if(argc<3 || argc >5){
    #ifdef DEBUG
    printf("%s\n", "error: número de parámetros excesivos o escasos");
    #endif
    return -1;
  }
  struct timeval inicio,fin;
  char* tipo;
  char* modo;
  /**
  Obtenemos parámetros en el caso de que sea una ejecución secuencial
  **/
  if(argc<4){
    longitud=atol(argv[1]);
    tipo=argv[2];
    /**
    Obtenemos parámetros en el caso de que sea un tipo de ejecución paralela
    **/
  }else if(strcmp((argv[1]),"--multi-thread")==0 && argc<6){
    modo=argv[1];
    nthreads=atoi(argv[2]);
    longitud=atol(argv[3]);
    tipo=argv[4];
    /**
    Obtenemos parámetros en el caso de que sea el otro tipo de ejecución paralela
    **/
  }else{
    longitud=atol(argv[1]);
    tipo=argv[2];
    modo=argv[3];
    nthreads=atoi(argv[4]);
  }
  /**
  Comprobamos que se haya introducido el tipo correcto
  **/
  if(strcmp(tipo,"sum")==1||strcmp(tipo,"sub")==1||strcmp(tipo,"xor")==1){
    #ifdef DEBUG
    printf("%s\n","error: tipo erroneo");
    #endif
    return -1;
  }
  /**
  Comprobamos que se haya introducido el número de threads correcto
  **/
  if(nthreads<1 || nthreads>12){
    #ifdef DEBUG
    printf("%s\n","error: numero de threads incorrecto");
    #endif
    return -1;
  }
  /**
    Reservamos espacio para una lista de tamaño igual a la longitud obtenida
    y rellenamos dicha lista con números de 0 a longitud-1
  **/
  double *lista=(double *) malloc(sizeof(double)*longitud);
  for(auto i=0;i<longitud;i++){
    lista[i]=i;
  }
  /**
    Comienza la parte más significativa (sea paralela o secuencial)
  **/
  gettimeofday(&inicio,NULL);
  /**
      Ejecución secuencial: se comprueba el tipo y se asigna a una función
      de las tres que tenemos para operar (funSum,funSub o funXor)
  **/
  if(nthreads==1){
    if(strcmp(tipo,"sum")==0){
      funSum(lista,longitud,0,0,0);
    }else if(strcmp(tipo,"sub")==0){
      funSub(lista,longitud,0,0,0);
    }else{
      funXor(lista,longitud,0,0,0);
    }
  /**
    Ejecución paralela
  **/
  }else{

    long longitudThread = floor(longitud/nthreads); //Asignamos la longitud de cada hilo
    int resto=longitud-nthreads*longitudThread; //Calculamos el resto
    std::thread threads[nthreads]; //Declaración el array de threads

    #ifdef FEATURE_LOGGER
    std::condition_variable cv2; //Variable condicional del main
    std::thread t_logger; //Declaración del hilo del logger
    if(strcmp(tipo,"xor")==0){
      t_logger = std::thread(procesa,&cv2,1); //Se asigna la función del thread en caso de ser una xor
    }else{
      t_logger = std::thread(procesa,&cv2,0); //Se asigna la función del thread en caso de no ser una xor
    }
    #endif
    /**
      Asignamos a cada thread del array una función dependiendo del tipo de operación que sea
    **/
    if(strcmp(tipo,"sum")==0){
      for (auto i=0; i<nthreads; ++i){
        threads[i] = std::thread(funSum,lista,longitudThread,i*longitudThread,resto,i);
      }
    }else if(strcmp(tipo,"sub")==0){
      for (auto i=0; i<nthreads; ++i){
        threads[i] = std::thread(funSub,lista,longitudThread,i*longitudThread,resto,i);
      }
    }else{
      for (auto i=0; i<nthreads; ++i){
        threads[i] = std::thread(funXor,lista,longitudThread,i*longitudThread,resto,i);
      }
    }
    for (auto i=0; i<nthreads; ++i){
      threads[i].join();
    }
    #ifdef FEATURE_LOGGER
    std::unique_lock<std::mutex> ulk(mut); //Creamos un lock que depende del mutex global
    cv2.wait(ulk, []{return ready2;}); //Se espera a que acabe de trabajar el hilo del logger
    ulk.unlock(); //Se libera el lock anterior
    #ifdef DEBUG
    printf("Fin thread logger \n");
    #endif
    t_logger.join();
    #endif
  }
  /**
    Se acaba la parte significativa ,se calcula cuanto ha tardado dicha parte y se imprime
  **/
  gettimeofday(&fin,NULL);
  double t = (double) ((fin.tv_sec  + fin.tv_usec/1000000.0) - (inicio.tv_sec + inicio.tv_usec/1000000.0)) ;
  printf("%f\n",t);
  #ifdef FEATURE_LOGGER
  /**
    Calculamos secuencialmente (para evitarnos errores) cuanto tendría que dar la ejecución
    y se comprueba que sea igual que lo que ha calculado el hilo del logger
  **/
  auxGlbl=0;
  for(auto i=0;i<longitud;i++){
    lista[i]=i;
  }
  if(strcmp(tipo,"sum")==0){
    for(auto i=0;i<longitud;i++){
      auxGlbl=auxGlbl+lista[i];
    }
  }else if(strcmp(tipo,"sub")==0){
    for(auto i=0;i<longitud;i++){
      auxGlbl=auxGlbl-lista[i];
    }
  }else{
    int aux=0;
    for(auto i=0;i<longitud;i++){
      aux=aux^(int)lista[i];
    }
    auxGlbl=aux;
  }
  printf("%f\n", result);
  printf("%f\n", auxGlbl);
  if(result==auxGlbl){ //Comprobación
    printf("El resultado es correcto \n");
    return 0;
  }else{
    printf("El resultado no es correcto \n");
    return 1;
  }
  #endif
  #ifdef FEATURE_OPTIMIZE
  //printf("%f\n",auxGlbl.load());
  #endif
  #ifndef FEATURE_OPTIMIZE
  //printf("%f\n",auxGlbl);
  #endif
  return auxGlbl;

}
#ifdef FEATURE_LOGGER
/**
  Función del logger

  Parámetros: variable condicional del main y el tipo de operación que es (xor u otro)

  Se comprueba que acaben todos los hilos de trabajar, si no se cumple eso se crea un lock
  dentro del bucle y se hace un wait en espera de que uno de los hilos de operación le haya
  puesto ready a true y de que haya hecho un notify_one al acabar. Cuando sale del wait, pone
  otra vez a false ready por si quedan hilos de operar y después libera el lock. Tras salir de
  este bucle cuando os hilos hayan acabado de operar se procede a sacar por pantalla en orden
  de creación el resultado de cada hilo. Después, se haya el resultado total se pone ready2 a true
  y se hace notify a la variable condicional del main.
**/
void procesa(std::condition_variable *cv2,int tip){
  while(contador!=nthreads){
    std::unique_lock<std::mutex> lk(mut);
    cv.wait(lk, []{return ready;});
    ready=false;
    lk.unlock();
  }
  for(int i=0;i<nthreads;i++){
    printf("Resultado calculado por el hilo %d es %f \n",i,listaAux[i]);
  }if(tip==0){
    for(int i=0;i<nthreads;i++){
      result=result+listaAux[i];
    }
  }else{
    int aux=0;
    for(int i=0;i<nthreads;i++){
      aux=aux^(int)listaAux[i];
    }
    result=aux;
  }
  ready2=true;
  printf("Resultado total = %f\n", result);
  cv2->notify_one();
}
#endif
/**
  A continuación, se encuentran 3 funciones de las cuales solo explicaré una ya que son
  muy similares. Me centrare en la primera funSum.

  Función que suma cada elemento del array lista.

  Parámetros: la lista sobre la que se encuentran los elementos a operar, el frag es el
  tamaño del fragmento del array sobre el que se va a operar, index es el índice del array
  sobre el que se opera, es decir, si un hilo comienza en lista[20] index sería 20; resto
  es el equivalente a frag en caso de que haya resto y por último num es el número de array
  según su orden de creación.
**/
void funSum(double* lista,long frag, long index, int resto, int num){
  double aux=0;
  for(auto i=0;i<frag;i++){
    aux=aux+lista[index+i];
  }
  #ifndef FEATURE_OPTIMIZE
  /**
    Se comprueba si hay resto, se crea un lock para garantizar exclusión mutua a la hora de operar
    con variables globales (resto y auxGlbl). Si hay resto y repite está a false quiere decir que es el
    primer hilo que ha acabado su parte y calcula el resto, para ello pone
  **/
  {
    std::lock_guard<std::mutex> guard(mut);
    if(resto!=0 && repite==false){
      #ifdef DEBUG
      printf("Ha entrado resto \n");
      #endif
      repite=true;
      for(auto i=longitud-resto;i<longitud;i++){
        aux=aux+lista[i];
      }
    }
    auxGlbl=auxGlbl+aux;
    /**
      Si hay FEATURE_LOGGER se guarda lo que ha calculado cada hilo en listaAux en orden de creación (esto se
      garantiza por el parámetro num), se hace contador++ indicando que el hilo ha acabado, pone ready a true y
      notifica a la variable condicional global.
    **/
    #ifdef FEATURE_LOGGER
    listaAux[num]=aux;
    contador++;
    ready=true;
    cv.notify_one();
    #endif
  }
  #endif
  /**
    Si FEATURE_OPTIMIZE está activo no usamos mutex ya que la exclusión mutua viene garantizada por los atómicos.
    Por lo que el código sería similar a la parte anterior sin el lock. Sin embargo, al usar dos variables globales
    dentro de un mismo lock, y una de ellas comprobarlas al inicio de éste. No puedo usar de una forma tan simple
    los atómicos por lo que me sigue prevaleciendo el lock en la primera parte del fragmento.
  **/
  #ifdef FEATURE_OPTIMIZE
  {
  std::lock_guard<std::mutex> guard(mut);
  if(resto!=0 && repite==false){
    repite=true;
    for(auto i=longitud-resto;i<longitud;i++){
      aux=aux+lista[i];
    }
  }
  }
  auxGlbl=auxGlbl+aux;
  #endif
}
void funSub(double* lista,long frag, long index, int resto,int num){
  double aux=0;
  for(auto i=0;i<frag;i++){
    aux=aux-lista[index+i];
  }
  #ifndef FEATURE_OPTIMIZE
  {
    std::lock_guard<std::mutex> guard(mut);
    if(resto!=0 && repite==false){
      #ifdef DEBUG
      printf("Ha entrado resto \n");
      #endif
      repite=true;
      for(auto i=longitud-resto;i<longitud;i++){
        aux=aux-lista[i];
      }
    }
    auxGlbl=auxGlbl+aux;
    #ifdef FEATURE_LOGGER
    listaAux[num]=aux;
    contador++;
    ready=true;
    cv.notify_one();
    #endif
  }
  #endif
  #ifdef FEATURE_OPTIMIZE
  {
  std::lock_guard<std::mutex> guard(mut);
  if(resto!=0 && repite==false){
    repite=true;
    for(auto i=longitud-resto;i<longitud;i++){
      aux=aux-lista[i];
    }
  }
  }
  auxGlbl=auxGlbl+aux;
  #endif
}
void funXor(double* lista,long frag, long index, int resto,int num){
  int aux=0;
  for(auto i=0;i<frag;i++){
    aux=aux^(int)lista[index+i];
  }
  #ifndef FEATURE_OPTIMIZE
  {
    std::lock_guard<std::mutex> guard(mut);
    if(resto!=0 && repite==false){
      #ifdef DEBUG
      printf("Ha entrado resto \n");
      #endif
      repite=true;
      for(auto i=longitud-resto;i<longitud;i++){
        aux=aux^(int)lista[i];
      }
    }
    auxGlbl=(int)auxGlbl^aux;
    #ifdef FEATURE_LOGGER
    listaAux[num]=aux;
    contador++;
    ready=true;
    cv.notify_one();
    #endif
  }
  #endif
  #ifdef FEATURE_OPTIMIZE
  {
  std::lock_guard<std::mutex> guard(mut);
  if(resto!=0 && repite==false){
    repite=true;
    for(auto i=longitud-resto;i<longitud;i++){
      aux=aux^(int)lista[i];
    }
  }
  }
  #endif
}
