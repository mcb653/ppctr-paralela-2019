#!/usr/bin/env bash
file=resultlog.csv
touch $file
for season in 1 2; do
    for benchcase in fo1-2 fo1-4 fo2-2 fo2-4  ; do
        echo $benchcase >> $file
        for i in `seq 1 1 20`;
        do
            ./runner2.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
