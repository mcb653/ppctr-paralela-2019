size1=300000
size2=300000000
op=sum
case "$1" in
    fo1-2)
        ./build/p1 $size1 $op --multi-thread 2
        ;;
    fo1-4)
        ./build/p1 $size1 $op --multi-thread 4
        ;;
    fo2-2)
        ./build/p1 $size2 $op --multi-thread 2
        ;;
    fo2-4)
        ./build/p1 $size2 $op --multi-thread 4
        ;;
esac
