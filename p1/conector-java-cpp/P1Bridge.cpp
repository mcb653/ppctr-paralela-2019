#include <jni.h>
#include "P1Bridge.h"
#include <cstdio>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <thread>
#include <math.h>
#include <mutex>
#include <time.h>
#include <sys/time.h>


void funSum(double* lista,long frag, long index, int resto, int num);
void funSub(double* lista,long frag, long index, int resto, int num);
void funXor(double* lista,long frag, long index, int resto, int num);
long longitud;
int nthreads=1;
double auxGlbl;
bool repite = false;
std::mutex mut;

int todo(long longitud,String tipo, String modo,int nthreads){
    struct timeval inicio,fin;

    if(strcmp(tipo,"sum")==1||strcmp(tipo,"sub")==1||strcmp(tipo,"xor")==1){
      return -1;
    }
    if(nthreads<1 || nthreads>12){
      return -1;
    }
    double *lista=(double *) malloc(sizeof(double)*longitud);
    for(auto i=0;i<longitud;i++){
      lista[i]=i;
    }
    gettimeofday(&inicio,NULL);

    if(nthreads==1){
      if(strcmp(tipo,"sum")==0){
        funSum(lista,longitud,0,0,0);
      }else if(strcmp(tipo,"sub")==0){
        funSub(lista,longitud,0,0,0);
      }else{
        funXor(lista,longitud,0,0,0);
      }
    }else{
      long longitudThread = floor(longitud/nthreads); //Asignamos la longitud de cada hilo
      int resto=longitud-nthreads*longitudThread; //Calculamos el resto
      std::thread threads[nthreads]; //Declaración el array de threads

      if(strcmp(tipo,"sum")==0){
        for (auto i=0; i<nthreads; ++i){
          threads[i] = std::thread(funSum,lista,longitudThread,i*longitudThread,resto,i);
        }
      }else if(strcmp(tipo,"sub")==0){
        for (auto i=0; i<nthreads; ++i){
          threads[i] = std::thread(funSub,lista,longitudThread,i*longitudThread,resto,i);
        }
      }else{
        for (auto i=0; i<nthreads; ++i){
          threads[i] = std::thread(funXor,lista,longitudThread,i*longitudThread,resto,i);
        }
      }
      for (auto i=0; i<nthreads; ++i){
        threads[i].join();
      }
    }

    gettimeofday(&fin,NULL);
    double t = (double) ((fin.tv_sec  + fin.tv_usec/1000000.0) - (inicio.tv_sec + inicio.tv_usec/1000000.0)) ;
    printf("Tarda %f\n",t);

    printf("El resultado es %f\n",auxGlbl);

    return auxGlbl;
}

void funSum(double* lista,long frag, long index, int resto, int num){
    double aux=0;
    for(auto i=0;i<frag;i++){
      aux=aux+lista[index+i];
    }
    {
      std::lock_guard<std::mutex> guard(mut);
      if(resto!=0 && repite==false){
        repite=true;
        for(auto i=longitud-resto;i<longitud;i++){
          aux=aux+lista[i];
        }
      }
      auxGlbl=auxGlbl+aux;
  }
}
  void funSub(double* lista,long frag, long index, int resto,int num){
    double aux=0;
    for(auto i=0;i<frag;i++){
      aux=aux-lista[index+i];
    }
    {
      std::lock_guard<std::mutex> guard(mut);
      if(resto!=0 && repite==false){
        repite=true;
        for(auto i=longitud-resto;i<longitud;i++){
          aux=aux-lista[i];
        }
      }
      auxGlbl=auxGlbl+aux;
  }
}
  void funXor(double* lista,long frag, long index, int resto,int num){
    int aux=0;
    for(auto i=0;i<frag;i++){
      aux=aux^(int)lista[index+i];
    }
    {
      std::lock_guard<std::mutex> guard(mut);
      if(resto!=0 && repite==false){
        repite=true;
        for(auto i=longitud-resto;i<longitud;i++){
          aux=aux^(int)lista[i];
        }
      }
      auxGlbl=(int)auxGlbl^aux;
  }
}
JNIEXPORT jint JNICALL Java_P1Bridge_compute(JNIEnv *env, jobject thisObj,jint longitud, jstring tipo, jstring modo, jint nthreads){

}
