public class P1 {
  public static void main(String[] args) {
    long longitud = Long.parseLong(args[0]);
    String tipo= args[1];
    String modo = args[2];
    int nthreads = Integer.parseInt(args[3]);
    P1Bridge p= new P1Bridge();
    int result = p.compute(longitud,tipo,modo,nthreads);
    System.out.println(result);
  }  
}
