size1=300000
size2=300000000
op=sum
case "$1" in
    st1)
        ./build/p1 $size1 $op
        ;;
    mt1-2)
        ./build/p1 $size1 $op --multi-thread 2
        ;;
    mt1-4)
        ./build/p1 $size1 $op --multi-thread 4
        ;;
    mt1-8)
        ./build/p1 $size1 $op --multi-thread 8
        ;;
    st2)
        ./build/p1 $size2 $op
        ;;
    mt2-2)
        ./build/p1 $size2 $op --multi-thread 2
        ;;
    mt2-4)
        ./build/p1 $size2 $op --multi-thread 4
        ;;
    mt2-8)
        ./build/p1 $size2 $op --multi-thread 8
        ;;
esac
