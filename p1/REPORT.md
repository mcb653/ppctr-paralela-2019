# P1: Multithreading en C++

Miguel Casamichana Bolado

Fecha 26/11/2019

## Prefacio

Objetivos conseguidos: recordar conocimientos básicos sobre C, aprender como para paralelizar bucles sencillos y aprender conceptos nuevos sobre atómicos, mutex, variables condicionales y sobre como hacer benchmarking.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre sistema operativo Ubuntu 19.04 64 bits:
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU AMD Ryzen 3 3200U with Radeon Veg
  - CPU 4 cores: 1 socket, 2 threads/core, 2 cores/socket.
  - CPU flags: pu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx mmxext fxsr_opt pdpe1gb rdtscp lm constant_tsc rep_good nopl nonstop_tsc cpuid extd_apicid aperfmperf pni pclmulqdq monitor ssse3 fma cx16 sse4_1 sse4_2 movbe popcnt aes xsave avx f16c rdrand lahf_lm cmp_legacy svm extapic cr8_legacy abm sse4a misalignsse 3dnowprefetch osvw skinit wdt tce topoext perfctr_core perfctr_nb bpext perfctr_llc mwaitx cpb hw_pstate sme ssbd sev ibpb vmmcall fsgsbase bmi1 avx2 smep bmi2 rdseed adx smap clflushopt sha_ni xsaveopt xsavec xgetbv1 xsaves clzero irperf xsaveerptr arat npt lbrv svm_lock nrip_save tsc_scale vmcb_clean flushbyasid decodeassists pausefilter pfthreshold avic v_vmsave_vmload vgif overflow_recov succor smca
  - CPU @2600 MHz (fijada con cpufreq usermode)
  - CPU: L2: 512K, L3: 4096K
  - 5'8 GiB RAM

  Primero con cpufreq-info observo las posibles frecuencias a las que puedo ajustar mi procesador.
  Veo que las 3 posibles son las siguientes: 2.60 GHz, 1.70 GHz, 1.40 GHz

  Establezco el modo de uso a userspace con:
  ```sh
  sudo cpufreq-set -r -g -userspace
  ```
  Establezco la frecuencia a 1.40 GHz con:
  ```sh
  cpufreq-set -r -f 1.40 GHz
  ```

  Realizo 100 ejecuciones de calentamiento con:

    ```sh
    for i in `seq 1 1 100`; do ./build/p1 300000 sum --multi-thread 8;done
    ```

  Tras esto, ejecuto un top para ver cuantos procesos se me están ejecutando y procedo a ejecutar el benchsuite.sh que mostraré más adelante.

  Cabe destacar que como mi frecuencia fluctúa bastante, he realizado 30 ejecuciones por estación en vez de 10, pero ya lo explicaré con detalle posteriormente.


## 2. Diseño e Implementación del Software

En primer lugar cabe destacar que en mi código está dividido en 3 grandes partes, me centraré compartiendo código solamente en los apartados que considere de cierta complejidad, el resto lo explicaré brevemente:

***Main***

En cuanto al main, lo primero es comprobar los parámetros obtenidos cuando se ejecuta el binario. Tras guardar dicha entrada de parámetros se reserva espacio en memoria para usando malloc almacenar una lista que pueda guardar x doubles donde x es la longitud recibida y la relleno de 1 a x.
```sh
double *lista=(double *) malloc(sizeof(double)*longitud);
```

 Tras esto, inicio la API gettimeofday ya que a partir de este instante es donde no se comparte código y puedo sacar conclusiones de rendimiento de mi ejecución secuencial respecto a mi ejecución paralela.
```sh
  gettimeofday(&inicio,NULL);
```
Posteriormente, clasifico en función del número de parámetros si es o no una ejecución multi-threading. En caso de no serlo se establece nthreads a 1 y se ejecuta el programa en secuencial llamando a la función del tipo de operación deseada. En caso de ser multi-threading, calculo la longitud que le pasaré a cada thread, la cual es la longitud total de la lista entre el número de hilos; el resto en caso de haberlo, el cual es la longitud total menos la longitud de cada hilo por el número de hilos y además declaro un array de threads de tamaño nthreads.

En caso de estar definido FEATURE_LOGGER se define una variable condicional llamada cv2 y se crea el thread del logger. Posteriormente, si la operación es una xor se define dicho hilo pasándole como parámetro la variable condicional recientemente creada y un 1;
```sh
  std::thread t_logger = std::thread(procesa,&cv2,1);
```
en caso de ser sum o sub, se le pasa un 0.

Acto seguido, defino un array de threads y compruebo que tipo de operación estamos tratando: "sum", "sub" o "xor" con la instrucción strcmp(). Defino cada thread del array asignándole a cada uno de ellos la función deseada. Ejemplo:
```sh
  threads[i] = std::thread(funSum,lista,longitudThread,i*longitudThread,resto,i);
  ```

Tras dicha declaración de threads, se hace un join a cada uno de éstos para cuando acaben de operar.

Si está definido FEATURE_LOGGER, se crea un lock sobre el mutex global para obtener dicho mutex hasta que se cumpla el siguiente wait, el cual depende de que le llegue un notify y ready2 esté a true para continuar (me centraré más adelante en el método del logger). Si se cumple esto, supondría que el hilo del logger habría acabado su trabajo, se liberaría el mutex con el unlock y como bien acabo de decir, se haría un join al hilo del logger ya que habría acabado.
```sh
std::unique_lock<std::mutex> ulk(mut);
cv2.wait(ulk, []{return ready2;});
ulk.unlock();
t_logger.join();
```
Posteriormente, ya habría acabado nuestra ejecución o bien paralela o bien secuencial, por lo que calcularíamos el tiempo transcurrido con la APi gettimeofday y se imprimiría el tiempo por pantalla.
```sh
  ggettimeofday(&fin,NULL);
  double t = (double) ((fin.tv_sec  + fin.tv_usec/1000000.0) - (inicio.tv_sec + inicio.tv_usec/1000000.0)) ;
  ```

Además, si FEATURE_LOGGER está definido calculamos secuencialmente (para evitarnos errores) cuanto tendría que dar la ejecución y se comprueba que sea igual que lo que ha calculado el hilo del logger. Se retornaría 0 en caso de que fuese correcto o 1 en caso de no serlo.

Finalmente, se retorna el resultado calculado.

***procesa***

Función correspondiente al hilo del logger, se encarga de comprobar que los hilos acaben, de imprimir sus fragmentos operados en el orden deseado (orden de creación).

Parámetros: variable condicional del main y el tipo de operación que es (xor u otro)

Se comprueba que acaben todos los hilos de trabajar, si no se cumple eso se crea un lock dentro del bucle obteniendo el mutex global y se hace un wait en espera de que uno de los hilos de operación le haya puesto ready a true y de que haya hecho un notify_one al acabar. Cuando sale del wait, pone otra vez ready a false por si quedan hilos de operar y después libera el mutex global deshaciendo el lock anterior. Cabe destacar que se hace contador++ cada vez que un hilo acaba de operar su trozo (lo desarrollaré más posteriormente en funSum)
```sh
while(contador!=nthreads){
  std::unique_lock<std::mutex> lk(mut);
  cv.wait(lk, []{return ready;});
  ready=false;
  lk.unlock();
}
```

Tras salir de este bucle cuando os hilos hayan acabado de operar se procede a sacar por pantalla en orden de creación el resultado de cada hilo. Después, se calcula el resultado total dependiendo de si es una xor u otra operación se pone ready2 a true y se hace notify a la variable condicional del main indicando que el hilo del logger ha finalizado.

***funSum***

En esta función (muy similar a las funciones funSub y funXor) cabe destacar que tenemos un mutex del cual hacemos uso de un lock_guard para tener exclusión mutua a la hora de leer y escribir en variables globales como repite, auxGlbl o cv, contador,ready y listaAux en caso de usar el FEATURE_LOGGER.

Se le pasan como parámetros la lista de la cual obtenemos nuestros elementos con los que operamos, la longitud de la lista a operar, un índice el cual es el puntero del array donde empezamos, el resto el cual es la longitud del resto a operar y un número el cual identifica el hilo de 0 a nthreads.

Lo primero que hace es usar una variable auxiliar para ir calculando iteración a iteración el resultado de sumar cada elemento del fragmento de la lista que le toca, en caso de ser multi-threading. Posteriormente, se establece el lock_guard como mencioné anteriormente y se pasa a comprobar si hay que calcular el resto.

Para ello, se comprueba que el parámetro resto sea distinto de 0 y que la variable global repite (inicialmente está a false) sea falsa. En ese caso hay resto, repite se pone a true para que no se vuelva a calcular ningún resto y se repite el proceso anterior de calcular la suma del fragmento pero con el fragmento correspondiente al resto anterior. Tras esto, haya o no resto se suma a auxGlbl el valor de aux obtenido.
```sh
{
std::lock_guard<std::mutex> guard(mut);
if(resto!=0 && repite==false){
  repite=true;
  for(auto i=longitud-resto;i<longitud;i++){
    aux=aux-lista[i];
  }
}
auxGlbl=auxGlbl+aux;
}
```
En caso de estar definido FEATURE_LOGGER se guarda el valor de aux además en una lista global cuyo puntero es el número que identifica al hilo en orden de creación pasado como parámetro. Se suma 1 al contador global indicando que este hilo ya ha hecho su trabajo, se pone la variable global ready a true y se notifica la variable condicional global para que el lock correspondiente salga del wait en caso de cumplirse ciertas condiciones (explicado anteriormente).

```sh
listaAux[num]=aux;
contador++;
ready=true;
cv.notify_one();
```
Cabe destacar que ésto se encuentra bajo el scope del lock_guard anterior ya que esas variables son globales y se debe garantizar que se acceden a ellas y se modifican en exclusión mutua.

***FEATURE_OPTIMIZE***

Cabe destacar que para la realización de este apartado opté por usar atómicos ya que son estructuras que te garantizan por si solas la exclusión mutua sobre variables globales (aquellas que declares como atómico) sobre las cuales se pueden realizar operaciones sencillas (sumas, restas, comparaciones ya que son restas al fin y al cabo) y te permiten eliminar los mutexes y por lo tanto optimizar el código.

Lo que opté por hacer fue hacer atómica mi variable global sobre la que guardaba mi resultado de cada hilo y de esta forma eliminar el mutex. Sin embargo, tal y como he hecho mi código, no puedo eliminar el mutex del todo ya que tengo otra variabke global llamada repite la cual es un booleano que me indica si un hilo cuando opera tiene que coger el resto o no. Esta es global y necesita exlcusión mutua a la hora de operar con ella. Opté por hacerla atómica también. Sin embargo, aumentaría la complejidad del problema y siguiendo lo que me sugeriste en tu despacho, opté por finalmente dejarla global gestionada por el lock del mutex.

De esta forma, solo he podido sacar del mutex las operaciones sobre el atómico global como se puede apreciar aquí:

```sh
#ifdef FEATURE_OPTIMIZE
{
std::lock_guard<std::mutex> guard(mut);
if(resto!=0 && repite==false){
  repite=true;
  for(auto i=longitud-resto;i<longitud;i++){
    aux=aux-lista[i];
  }
}
}
auxGlbl=auxGlbl+aux;
#endif
```

## 3. Metodología y desarrollo de las pruebas realizadas

A la hora de realizar mi benchmarking he usado el modo userspace y he fijado la frecuencia de mi procesador a la más baja posible para poder aumentar el tiempo de ejecución de mis ejecuciones y que de esta forma fuese más apreciable los cambios entre tamaños de array, número de hilos, etc. La frecuencia usada ha sido

Lo primero de todo, quiero explicar que he tenido un problema de fluctuaciones de frecuencias a la hora de realizar el benchmarking. Por ello, he realizado unas cuantas ejecuciones más en cada estación y he comprobado como aún así seguían siendo un poco irregulares los resultados entre estación y estación (las diferencias no son muy grandes, pero tampoco despreciables del todo). Es por ello que he usado dos estaciones para comprobar dichas fluctuaciones. Además, previamente a ejecutar los benchsuites he realizado una ejecución unas 100 veces a modo de calentamiento. Posteriormente, ejecuto los benchsuites que muestro a continuación. Uso dos, el primero para mostrar los resultados de la ejecución paralela frente a la ejecución secuencial y el segundo para mostrar los tiempos de ejecución con el modo FEATURE_OPTIMIZE activo.


**benchsuite.sh:**

```sh
#!/usr/bin/env bash
file=resultlog.csv
touch $file
for season in 1 2; do
    for benchcase in st1 mt1-2 mt1-4 mt1-8 st2 mt2-2 mt2-4 mt2-8 ; do
        echo $benchcase >> $file
        for i in `seq 1 1 20`;
        do
            ./runner.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
```
Tengo en cuenta un tamaño no muy excesivo como puede ser 300000 para poder ver si el secuencial puede ser más óptimo con tamaños de operación "normales" y un tamaño excesivo para ver si realemente hay ganancia de la ejecución paralela frente a la secuencial. Además como tengo 4 cores, pruebo ejecutando con 2, 4 y 8 cores; 12 no tendría ningún sentido.


**runner.sh:**
```sh
size1=300000
size2=300000000
op=sum
case "$1" in
    st1)
        ./build/p1 $size1 $op
        ;;
    mt1-2)
        ./build/p1 $size1 $op --multi-thread 2
        ;;
    mt1-4)
        ./build/p1 $size1 $op --multi-thread 4
        ;;
    mt1-8)
        ./build/p1 $size1 $op --multi-thread 8
        ;;
    st2)
        ./build/p1 $size2 $op
        ;;
    mt2-2)
        ./build/p1 $size2 $op --multi-thread 2
        ;;
    mt2-4)
        ./build/p1 $size2 $op --multi-thread 4
        ;;
    mt2-8)
        ./build/p1 $size2 $op --multi-thread 8
        ;;
esac
```

Ejecutando este benchsuite a continuación del anterior almaceno toda la información respectiva al modo FEATURE_OPTIMIZE en el mismo fichero de tipo csv.

**benchsuite2.sh:**

```sh
#!/usr/bin/env bash
file=resultlog.csv
touch $file
for season in 1 2; do
    for benchcase in fo1-2 fo1-4 fo2-2 fo2-4  ; do
        echo $benchcase >> $file
        for i in `seq 1 1 20`;
        do
            ./runner2.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
```
Uso otra vez los mismos tamaños para corresponder a los resultados obtenidos en los casos multithread obtenidos anteriormente y establezco como número de threads 2 y 4 ya que considero que son los casos más significativos.

**runner2.sh:**
```sh
size1=300000
size2=300000000
op=sum
case "$1" in
    fo1-2)
        ./build/p1 $size1 $op --multi-thread 2
        ;;
    fo1-4)
        ./build/p1 $size1 $op --multi-thread 4
        ;;
    fo2-2)
        ./build/p1 $size2 $op --multi-thread 2
        ;;
    fo2-4)
        ./build/p1 $size2 $op --multi-thread 4
        ;;
esac
```
De cada 20 ejecuciones, he desechado la primera y tenido en cuenta las otras 19.

Estos son los resultados obtenidos tras la ejecución de benchsuite.sh:

![](images/resultados1.png)

y aquí se encuentran los resultados obtenidos tras la ejecución de benchsuite2.sh:

![](images/resultados2.png)

Como se puede apreciar, en cuanto a la ejecución secuencial frente a la paralela con un tamaño "mediano" el speed-up obtenido sobre la ejecución secuencial es ligeramente superior a excepción del caso de 4 threads en el que ya ronda un speed-up de 2. Sin embargo, lo contrastamos frente al speed-up obtenido con un tamaño "grande" para este procesador (a partir de un tamaño de 300000000 no puede operar) y vemos como hay un mayor speed-up en cada una de las ejecuciones. Es decir, con el primer tamaño de array y con 2 hilos obtenemos un speed-up de 1.14 mientras que con el segundo tamaño y mismo número de hilos obtenemos un speed-up de 1.98, casi el doble. Esto se debe a que a mayor longitud de array, mejor se aprovecha el paralelismo ya que permite que el realizar una división de trabajo entre hilos se optimiza en grandes medidas el tiempo de ejecución y de cómputo del programa.

De hecho, las pruebas que he realizado han sido con la intención de contrastar como a mayor tamaño de array, mayor optimización obtenida tras aplicar paralelismo. También, cabe destacar que si hubiese usado un tamaño aún mucho más pequeño, cosa que creo innecesaria debido a que es algo que considero obvio, hubiese obtenido que el tiempo de ejecución secuencial hubiese sido menor que el de las ejecuciones multi-threading por lo que sus respectivos speed-ups sobre la secuencial serían <1.

Esto se debe, a que el crear threads, sincronizarlos haciendo uso de mutexes, variables condicionales, etc, lleva un tiempo que pese a ser "despreciable" en ejecuciones grandes, en ejecuciones pequeñas puede llegar a tener un peso tan grande o incluso mayor, que el tiempo de ejecución del programa.

A continuación muestro las tablas generadas:

![](images/Gráfica1:Par-Sec.png)

![](images/Gráfica2:Par-Sec.png)

Estas dos primeras gráficas muestran las ganancias de la ejecución paralela frente a la secuencial. Cada una con un tamaño distinto como bien su subtítulo indica.

![](images/Gráfica3:Opt-Par.png)

![](images/Gráfica4:Opt-Par.png)

Estas otras muestran las ganancias de la ejecución en modo FEATURE_OPTIMIZE frente a la paralela. Cada una con un tamaño distinto como bien su subtítulo indica.

Como se puede apreciar la ejecución multi-thread y la optimize es muy similar en cuanto a tiempos de ejecución (speed-ups de n hilos optimize sobre n hilos multi-thread es igual a 1 prácticamente) ya que como he explicado antes por el uso de dos variables globales condicionadas por un mismo lock de un mutex no he podido eliminar del todo dicho mutex, por lo que al fin y al cabo no está optimizada del todo. Sin embargo, en caso de optimizarla del todo mediante atómicos, tampoco habría mucho speed-up ya que creo que el rendimiento de una ejecución multi hilo es la división de trabajo lo cual hace que la sincronización de hilos sea un tiempo despreciable en ejecuciones grandes. 

## 4. Discusión

*Explica cómo has hecho los benchmarks y qué metodología de medición has usado
(ROI+gettimeofday, externo).*

Como he hecho los benchmarks viene explicado en el apartado anterior. Sin embargo, aún no he explicado por que uso ROI+gettimeofday. La razón del uso de esta metodología viene dada por que en este caso quiero comparar la región de interés del programa. Es decir, no quiero comparar el tiempo de ejecución total del programa cuando es una ejecución secuencial frente a cuando es una paralela. Sino, que quiero comparar la diferencia de la parte secuencial frente a la paralela. Pongamogs por ejemplo que hay una parte del código común que me tarda 2 segundos, la parte de ejecución secuencial me tarda 3 y la paralela 1. No es lo mismo un speed-up de 3/1 que de 5/3. Es por esta razón por la que quiero comprobar el tiempo de ejecución de la región de interés y no la total.
