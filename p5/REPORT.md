# Práctica 5

Miguel Casamichana Bolado

Fecha 18/12/2019

## Prefacio

Esta práctica me ha servido para repasar directivas básicas de openMP aplicadas en la práctica 3 y a usar directivas no usadas hasta el momento como podría ser critical u otras directivas. Además,la carga de trabajo de esta práctica me ha parecido mucho menor que en otras prácticas como la 1 y mucho más entretenida.

## Índice

1. Sistema
2. Resolución de ejercicios planteados
3. Metodología y desarrollo de las pruebas realizadas

## 1. Sistema

Todos los datos de mi pc vienen proporcionados en la práctica 1 y las pruebas previas al lanzamiento de mis benchmarks también, por lo que en el apartado 3 me limitaré a explicar mis benchmarks brevemente y comentar los resultados obtenidos.


## 2. Resolución de ejercicios planteados

Ejercicio 1: Usando profiling podemos ver que claramente el método a parelizar es explode, ya que es el método que más veces es llamado y que más tiempo de ejecución lleva. Para ello, he probado cambiando n con valores de entre 300 a 3000 y count_max con todo tipo de valores; y se ve como a más tamaño, aumenta un poco más el porcentaje de tiempo de ejecución del método. Está en torno al 73% de utilización.

Cabe destacar que he usado valgrind, para ello he creado un apartado en el make llamado profiling con el cual cuando compilo usando estas dos líneas de compilación:
```sh
gcc -pthread -o build/p5 src/p5.c -lm -pg ;
valgrind --tool=callgrind --dump-instr=yes ./build/p5 ;
```
Con la primera se compila el archivo.c permitiendo posteriormente aplicar valgrind, y con la otra se crea el archivo callgrind. Cuando abres el archivo generado .callgrind puedo proceder a que método es el que más tiempo de ejecución está usando.

En esta imagen se puede apreciar todo lo comentado anteriormente:

 ![](images/callgrind.png)

Para ver cuantas líneas de código no son usadas realizo un test de cobertura. Para ello, he creado un apartado en el Makefile llamado cobertura el cual tiene 3 líneas:
```sh
gcc -fprofile-arcs -ftest-coverage -g -o build/p5 src/p5.c -lm ;
./build/p5 ;
gcov p5.gcno ;
```

con la primera compilo el archivo .c y se me genera un archivo .gcno, con la segunda línea ejecuto simplemente el ejecutable y con la tercera se me crea el archivo .gcov. Al abrir el archivo .gcov se me permite ver que líneas de código se utilizan más (son más veces llamadas o utilizadas). Las líneas que aparezcan con una # son aquellas que no han sido usadas.

Una vez explicado, esto cabe destacar que las líneas que no se usan son aquellas que se ejecutarían en caso de error, ya sea por haber introducido algún argumento, en caso de que no se haya podido abrir el archivo, etc.

Ejercicio 2: Comenzamos usando una etiqueta parallel para poder crear una región paralela sobre la que trabajar en la que especificamos que variables van a ser privadas, cuales compartidas y cuantos números de hilos deseamos usar. Las variables privadas son aquellas que son únicas para cada hilo como pueden ser los índices de los bucles, las variables compartidas son iguales para todos los hilos y fijas; y el número de hilos es nthreads que es pasado como parámetro.

Dentro de esta región paralela creamos un omp for para poder paralelizar el bucle entre hilos, de esta forma las iteraciones del bucle se dividen entre todos los hilos dependiendo del tipo de planificador, logrando así una correcta división de trabajo. Además, he especificado que el for sea dinámico ya que de esta forma es más óptimo debido hay una línea de código dentro del explode que está condicionada por un if que no siempre se ejecuta, es por esto que si se hiciese estático se da el caso de que la carga de trabajo no es igual ya que ciertas llamadas a este método ejecutan esa línea de código y otras llamadas no.

Ejercicio 3: Primero, defino otra región paralela muy similar a la anterior que engloba a los siguientes 4 apartados, cada uno de ellos condicionados en función de la compilación condicional generada en función del valor de IMPLEM.

a) Implementación mediante directivas OpenMP.

Para garantizar la sección crítica, dentro de un omp for he utilizado la etiqueta critical la cual garantiza que la sección limitada por los scopes de esta directiva solo puedan ser accedidos por un solo hilo a la vez, es decir, garantiza que esa sección limitada por scopes sea una sección crítica.

b) Implementación mediante funciones de runtime.

Para garantizar aquí la sección crítica, hemos hecho uso de funciones de runtime. Para ello definimos un lock y lo inicializamos con con omp_init_lock. Acto seguido, entramos dentro del omp for, como en el caso anterior, cogemos el lock antes de la sección crítica, y bloqueamos frente a otros hilos por así decirlo, como si fuese un lock y un mutex de C++. Cuando salimos de la sección crítica hacemos un omp_unset_lock para permitir que otro hilo entre en la sección crítica. Por último, una vez fuera de la región paralela, destruimos el lock con omp_destroy_lock.

c) Implementación secuencial de esa parte del código.

Si realizamos una ejecución secuencial, es decir, que un solo hilo entra en la sección crítica, ésta ya está protegida frente a otros hilos ya que obviamente solo hay uno. Por ello, he usado una directiva omp single por la cual solo nos entra el primer hilo disponible dentro de la región paralela, es decir, el que primero acabe coge esa región definida por los scopes del omp single.

d) Implementación con variables privadas a cada thread y selección del máximo de todas ellas.

Para garantizar la sección crítica en este apartado he optado por usar un reduction sobre la variable c_max. Reduction actúa como un mutex en C++ sobre las variables que introduzcas dentro garantizando exclusión mutua sobre estas variables.

## 3. Metodología y desarrollo de las pruebas realizadas

He fijado la N a 1000 como viene por defecto ya que creo que me genera unos tiempos de ejecución apreciables para la medición.

Además, a parte de dar permisos a los scripts usando chmod +x más el nombre del archivo .sh, he realizado un make all para compilar todos los archivos según la compilación condicional y crear así 5 ejecutables distintos: el secuencial original, el ejecutable correspondiente al apartado a, otro para el b, otro para el c y un último ejecutable para el apartado d. Al igual que he usado 2 estaciones por las mismas razones explicadas en la práctica 1 y 2, las mismas ejecuciones por estación (20+1 de warm up) debido a las grandes fluctuaciones que tengo (explicado también en la práctica 1 y 2 junto con como asigno los governors de cada core y sus respectivas frecuencias).

Una vez introducido esto, el archivo benchsuite.sh es bastante similar a cualquiera de las otras prácticas por lo que no me centraré en explicarle sino en centrarme en que datos he usado y por qué.

He optado por usar la N mencionada anteriormente y el máximo número de threads de mi equipo (por defecto, en caso de no especificar el número de threads, es decir, que no es pasado como parámetro ni como variable de entorno, se coge el número máximo de hilos disponibles del pc) ya que con un tamaño de 1000 se puede apreciar de forma clara los speed-up de la sección crítica y al usar el máximo número de hilos se consigue un mayor speed-up con un tamaño aceptable (explicado en la práctica 1 y 2 como un tamaño muy pequeño favorece ejecuiones secuenciales frente a paralelas).

Tras la ejecución de mi script obtengo los siguientes speed-ups:

![](images/grafica.png)

obtenidos de los siguientes resultados

![](images/results.png)

donde la tabla de arriba son los tiempos de ejecución medios de cada tipo de ejecución y la de abajo son los speed-ups de la tabla.

Cabe destacar que la ejecución a, el que corresponde al uso de la directiva critical es la menos eficiente y con gran diferencia. Esto puede ser debido al overhead producido por esta sección crítica aplicando dicha directiva ya que por así decirlo una operación que repite cada thread constantemente estaría controlada como por una especie de mutex por así decirlo.

En cuanto al speed-up usando locks y unlocks propios de la ejecución también hay un speed-up negativo de casi 0,5. Esto puede ser debido a al igual que en caso anterior se produce mucho overhead pero menos que en el caso anterior. Debido a un constante uso del lock.

En cuanto al speed-up del apartado c, propio de usar la directiva single haciéndolo secuencial se ve que obviamente queda un poco inferior a 1 ya que es una ejecución secuencial, realizada por un solo hilo. Sin embargo, hay que añadirle el pequeño overhead producido por usar la directiva single y estar dentro de una sección paralela. Por esto, queda en torno a 0,95.

Por último, en cuanto al speed-up del apartado d, propio de usar la cláusula reduction dentro del bucle for aplicando que siempre se guarde el máximo valor con max, podemos ver como tiene un speed-up de unos 3,2. Esto se debe, a que a diferencia de los apartados a y b, la sección crítica en esta solo se encontraría cuando se opera con la variable c_max(En los otros casos la sección crítica eran dos líneas de código). Por así decirlo, sería como un atómico sobre el que todos los hilos pueden operar lo cual permite una gran optimización frente a la ejecución secuencial.
