#!/usr/bin/env bash
file=resultlog.csv
touch $file
for season in 1 2; do
    for benchcase in seq a b c d; do
        echo $benchcase >> $file
        for i in `seq 1 1 21`;
        do
            ./runner.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
